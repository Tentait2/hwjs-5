
// Опишіть своїми словами, що таке метод об'єкту
// По простому метод объекта это название функции которая находится в объекте

// Який тип даних може мати значення властивості об'єкта?
// Свойство объекта это значение любого типа принадлежащего объекту.
// Об'єкт це посилальний тип даних. Що означає це поняття?
// За каждым объектом остаётся часть памяти. Лучше всего это понятно когда мы пытаемся клонировать объект но получаем лишь ссылку на предыдущий объект


// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

function createNewUser(){
    const firstName = prompt('Enter your Name', 'Maxim');
    const secondName = prompt('Enter your second name', 'Lavrus');
    let newUser = {
        firstName,
        secondName,
        getLogin(){
            return `${firstName[0].toLowerCase()}${secondName.toLowerCase()}`
        }
    }
    return newUser
};
const user = createNewUser()
console.log(user)
console.log(user.getLogin())



